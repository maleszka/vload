local M = {}

-- Dependency lua modules
local data = require'vload/data'
local util = require'vload/util'

-- Local utilities
-- Return true if the mod_name is loaded, false otherwise
local function is_loaded(mod_name, bufnr)
  if data.mods_info[mod_name]['local'] then
    if data.mods_rtinfo[mod_name].loaded[bufnr] == true then return true else return false end
  end
  return data.mods_rtinfo[mod_name].loaded
end

-- Set loaded status to true
local function set_loaded(mod_name, bufnr)
  if data.mods_info[mod_name]['local'] then
    data.mods_rtinfo[mod_name].loaded[bufnr] = true
  else
    data.mods_rtinfo[mod_name].loaded = true
  end
end

-- Set loaded status to false
local function unset_loaded(mod_name, bufnr)
  if data.mods_info[mod_name]['local'] then
    data.mods_rtinfo[mod_name].loaded[bufnr] = nil
  else
    data.mods_rtinfo[mod_name].loaded = false
  end
end

-- Recursively loads modules and resolves dependency cycles
-- If mod_name is a global, either ft or bufnr must be nil
local function load_mod_rec(mod_name, ft, bufnr)
  -- Do not load module if already loaded
  if is_loaded(mod_name, bufnr) == true then
    return false
  end

  -- Protect from loading local modules as dependencies of global ones
  if ft == nil or bufnr == nil then
    if data.mods_info[mod_name]['local'] then
      util.notify("Cannot load " .. mod_name .. " when neither filetype nor bufnr is specified", 3)
      return false
    end
  elseif data.mods_info[mod_name]['local'] == true and
    -- Check filetype compatibility
    data.mods_info[mod_name].filetypes[ft] ~= true then
    util.notify(mod_name .. "Cannot load " .. mod_name " that does not match filetype " .. ft, 1)
    return false
  end

  -- Mark as loaded (it is not true but prevents graph cycles)
  set_loaded(mod_name, bufnr)

  -- Failure indicator
  local failure = false

  -- Check if module can be load
  if data.mods_rtinfo[mod_name].sourced == false and vim.fn.filereadable(data.mods_info[mod_name].file) == 0 then
    util.notify("Cannot read " .. data.mods_info[mod_name].file .. "! File is not readable", 3)
    failure = true
    goto ret
  end

  -- Iterate through dependencies
  if data.mods_info[mod_name].deps ~= nil then
    for _,dep_name in ipairs(data.mods_info[mod_name].deps) do
      load_mod_rec(dep_name, ft, bufnr)
    end
  end

  -- Actually load module
  -- Log
  util.notify("Loading " .. mod_name, 1)
  -- Source
  if data.mods_rtinfo[mod_name].sourced == false then
    vim.cmd('source ' .. data.mods_info[mod_name].file)
    data.mods_rtinfo[mod_name].sourced = true
  end
  -- Load
  if data.mods_info[mod_name]['local'] then
    vim.fn['g:Module_' .. mod_name .. '_load'](bufnr)

    -- Register to bf_mods
    if data.bf_mods[bufnr] == nil then data.bf_mods[bufnr] = {} end
    data.bf_mods[bufnr][mod_name] = true
  else
    vim.fn['g:Module_' .. mod_name .. '_load']()
  end

  ::ret::
  if failure then
    -- Undo loaded mark
    unset_loaded(mod_name, bufnr)

    -- Return failure
    return false
  else
    -- Return success
    return true
  end
end

-- Loads all globals respecting session weight
function M.load_globals()
  -- Log
  util.notify(" === Loading globals ===", 1)

  -- Check if cache has been loaded
  if data.mods_info == nil or data.vload_weight == nil then
    util.notify("Cannot load globals, because cache has not been loaded", 3)
    return false
  end

  -- Load each global module
  for _, glob_name in pairs(data.globals) do
    if data.mods_info[glob_name].priority >= 5 - data.vload_weight then
      load_mod_rec(glob_name)
    end
  end

  -- Success
  util.notify(" === Globals loaded successfully ===", 1)
  return true
end

function M.load_locals(bufnr)
  util.notify(" === Loading mods for " .. bufnr .. " ===", 1)

  -- Check if cache has been loaded
  if data.mods_info == nil or data.vload_weight == nil then
    util.notify("Cannot load local modules, because cache has not been loaded", 3)
    return false
  end

  -- Get filetype
  local ft = vim.api.nvim_buf_get_option(bufnr, 'filetype')

  -- Load each local module
  if data.ft_mods[ft] ~= nil then
    for _,mod_name in ipairs(data.ft_mods[ft]) do
      if data.mods_info[mod_name].priority >= 5 - data.vload_weight then
        load_mod_rec(mod_name, ft, bufnr)
      end
    end
  end

  -- Success
  util.notify(" === Buffer configured successfully ===", 1)
  return true
end

-- Simple and save loader exposed to user
function M.load_mod(mod_name)
  -- Check if cache has been loaded
  if data.mods_info == nil or data.vload_weight == nil then
    util.notify("Cannot load module, because cache has not been loaded", 3)
    return false
  end

  -- Check if module exists
  if data.mods_info[mod_name] == nil then
    util.notify("Cannot load " .. mod_name .. "! Module not added to the data", 3)
    return false
  end

  -- Set bufnr and ft
  local ft = nil
  local bufnr = nil
  if data.mods_info[mod_name]['local'] == true then
    -- Bufnr
    bufnr = vim.api.nvim_get_current_buf()

    -- Ft
    ft = vim.api.nvim_buf_get_option(bufnr, 'filetype')
  end

  -- Load module dependencies
  load_mod_rec(mod_name, ft, bufnr)
end

-- Recursively unloads mod_name and it's dependencies.
local function unload_mod_rec(mod_name, bufnr)
  -- Do not unload global modules
  if data.mods_info[mod_name]['local'] == false then
    util.notify("Cannot unload module " .. mod_name .. " because it is global", 1)
    return false
  end

  -- Cannot unload a not loaded module
  if is_loaded(mod_name, bufnr) == false then return false end

  -- Mark the current module as unloaded
  unset_loaded(mod_name, bufnr)

  -- Unload all dependencies
  if data.mods_info[mod_name].deps ~= nil then
    for _,dep_name in ipairs(data.mods_info[mod_name].deps) do
      -- Do not unload module if it can work independently
      if data.mods_info[dep_name].priority >= 5 - data.vload_weight then goto continue end

      unload_mod_rec(dep_name, bufnr)

      ::continue::
    end
  end

  -- Actually unload module
  -- Log
  util.notify("Unloading " .. mod_name, 1)
  -- Call unloader
  vim.fn['g:Module_' .. mod_name .. '_unload'](bufnr)
  -- Unregister from bf_mods
  data.bf_mods[bufnr][mod_name] = nil
  if vim.tbl_isempty(data.bf_mods[bufnr]) then data.bf_mods[bufnr] = nil end

  -- Success
  return true
end

function M.unload_locals(bufnr)
  util.notify(" === Unloading mods for " .. bufnr .. " ===", 1)

  -- Check if buffer has some loaded modules
  if data.bf_mods[bufnr] == nil then
    util.notify("Nothing to unload", 1)
    util.notify(" === Buffer deleted successfully ===", 1)
    return true
  end

  -- Backup loaded modules
  local mods_to_unload = data.bf_mods[bufnr]

  -- Unload each module
  for mod_name,_ in pairs(mods_to_unload) do
    unload_mod_rec(mod_name, bufnr)
  end

  -- Success
  util.notify(" === Buffer deleted successfully ===", 1)
  return true
end

-- Simple and save unloader exposed to user
function M.unload_mod(mod_name)
  -- Check if cache has been loaded
  if data.mods_info == nil or data.vload_weight == nil then
    util.notify("Cannot load globals, because cache has not been loaded", 3)
    return false
  end

  -- Check if module exists
  if data.mods_info[mod_name] == nil then
    util.notify("Cannot unload " .. mod_name .. "! Module not added to the data", 3)
    return false
  end

  -- Set bufnr
  local  bufnr = vim.api.nvim_get_current_buf()

  -- Load module dependencies
  unload_mod_rec(mod_name, bufnr)
end

-- Expose module functionality
return M
