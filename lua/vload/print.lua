local M = {}

-- Dependency lua modules
local data = require'vload/data'

-- Local utilities
-- Return true if the mod_name is loaded, false otherwise
local function is_loaded(mod_name, bufnr)
  if data.mods_info[mod_name]['local'] then
    if data.mods_rtinfo[mod_name].loaded[bufnr] == true then return true else return false end
  end
  return data.mods_rtinfo[mod_name].loaded
end

-- === Vars ===
function M.get_weight()
  return data.vload_weight
end

-- === Lists ===
-- Loaded: all local and globals modules that have been loaded
function M.get_loaded(bufnr)
  local list = {}
  local pos = 1

  -- Iterate all registered modules
  for mod_name,_ in pairs(data.mods_info) do
    if is_loaded(mod_name, bufnr) == true then
      list[pos] = mod_name
      pos = pos + 1
    end
  end

  return list
end

-- Loadable: unloaded globals and valid locals
function M.get_loadable(bufnr)
  local list = {}
  local pos = 1
  local ft = vim.api.nvim_buf_get_option(bufnr, 'filetype')

  -- Iterate all registered modules
  for mod_name,_ in pairs(data.mods_info) do
    if is_loaded(mod_name, bufnr) == false then
      if data.mods_info[mod_name]['local'] == true and data.mods_info[mod_name].filetypes[ft] ~= true then
        goto continue
      end

      -- Add module to list
      list[pos] = mod_name
      pos = pos + 1
    end
    ::continue::
  end

  return list
end

-- Unloadable: loaded local modules
function M.get_unloadable(bufnr)
  local list = {}
  local pos = 1

  if data.bf_mods[bufnr] ~= nil then
    for mod_name,_ in pairs(data.bf_mods[bufnr]) do
      list[pos] = mod_name
      pos = pos + 1
    end
  end

  return list
end

return M
