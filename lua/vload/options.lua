local M = {}

-- An alternative to the vim.fn.get
local function get(scope, var, default)
  if scope[var] == nil then
    return default
  end
  return scope[var]
end

-- This is a path to the file to which complete log information should be sent. If it's set
-- to nil or the file is non-writeable, only vim messages are used.
M.log_file = get(vim.g, 'vload#log_file', nil)

-- This is the verbosity of vimLoader for vim messages:
  -- 0: quiet
  -- 1: errors
  -- 2: warnings
  -- 3: info/debug
M.verbosity = get(vim.g, 'vload#verbosity', 1)

-- Custom path for vimLoader cache.
M.cache_file = get(vim.g, "vload#cache_file", vim.fn.stdpath("cache") .. "/vload.cache")

return M
