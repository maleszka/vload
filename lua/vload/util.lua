-- Prepare workspace scope
local M = {}
local options = require'vload/options'

-- Outputs log info to the file (or as vim.notify)
-- Severity: 1 - info, 2 - warning, 3 - error
function M.notify(msg, severity)
  -- Correct severity
  if severity == nil then severity = 2
  elseif severity > 3 then severity = 3
  elseif severity < 1 then severity = 1
  end

  local sev_name = {[1] = 'info', [2] = 'warning', [3] = 'ERROR'}

  -- Print to vim messages
  if severity > 3 - options.verbosity then
    vim.notify('[vload] ' .. sev_name[severity] .. ': ' .. msg, severity)
  end

  -- Output to the log_file.
  if options.log_file ~= nil then
    -- Open file
    local file = io.open(options.log_file, "a")

    if file ~= nil then
      -- Output
      file:write("[" .. vim.fn.strftime("%y%m%d %T") .. "] " .. sev_name[severity] .. ": " .. msg .. "\n")

      -- Close file
      file:close()
    end
  end
end

-- Returns true if file exists, it's readable and if it's writeable
function M.file_exists(path)
  local file = io.open(path, "r")

  -- File exists
  if file ~= nil then
    file:close()
    return true
  end

  -- File does not exists.
  return false
end

-- Shifts list table after position deletion
function M.shift_list(list)
  local size = #list

  local copy_list = {}
  local copy_pos = 1

  -- Shift list
  for pos = 1,size,1 do
    if list[pos] ~= nil then
      copy_list[copy_pos] = list[pos]
      copy_pos = copy_pos + 1
    end
  end

  -- Return the new list
  return copy_list
end

-- Expose module functionality
return M
