-- Prepare workspace scope
local M = {}

local util = require'vload/util'
local data = require'vload/data'
local loader = require'vload/loader'

local function define_commands()
  -- ModLoad - loads specified module and its dependencies
  vim.cmd([[
  function! g:VimLoader_ModLoad_comp(ArgLead, CmdLine, CursorPos)
    return luaeval("require('vload.print').get_loadable")(bufnr(''))
  endfunction

  command! -nargs=1 -complete=customlist,g:VimLoader_ModLoad_comp ModLoad lua require('vload/loader').load_mod('<args>')
  ]])
  -- ModUnload - unloads specified module and its dependencies
  vim.cmd([[
  function! g:VimLoader_ModUnload_comp(ArgLead, CmdLine, CursorPos)
    return luaeval("require('vload.print').get_unloadable")(bufnr(''))
  endfunction

  command! -nargs=1 -complete=customlist,g:VimLoader_ModUnload_comp ModUnload lua require('vload/loader').unload_mod('<args>')
  ]])
  -- ModWeight - changes session weight and reloads modules respecting
  -- specified weight
  vim.cmd([[
  command! -nargs=1 ModWeight lua require('vload').set_weight(<args>)
  ]])
  -- ModCache - regenerates modules database cache
  vim.cmd([[
  command! ModCache lua require('vload/data').gen_cache()
  ]])
  -- ModGetWeight
  vim.cmd([[
  command! ModGetWeight echo "VimLoader session weight:" luaeval("require('vload/print').get_weight")()
  ]])
  -- ModGetLoaded
  vim.cmd([[
  command! ModGetLoaded echo "Loaded modules: " luaeval("require('vload/print').get_loaded")(bufnr(''))
  ]])
  -- ModGetLoadable
  vim.cmd([[
  command! ModGetLoadable echo "Loadable modules:" luaeval("require('vload/print').get_loadable")(bufnr(''))
  ]])
  -- ModGetUnloadable
  vim.cmd([[
  command! ModGetUnloadable echo "Unloadable modules:" luaeval("require('vload/print').get_unloadable")(bufnr(''))
  ]])
end

function M.setup(mdirs, weight)
  -- Correct weight
  if weight > 4 then weight = 4 elseif weight < 0 then weight = 0 end

  -- Log information
  util.notify("vimLoader started with args:", 1)
  util.notify("  mdirs: " .. vim.inspect(mdirs), 1)
  util.notify("  weight: " .. weight, 1)

  -- Load data
  data.load_data(mdirs, weight)

  -- Source global modules
  loader.load_globals()

  -- Create user commands
  define_commands()

  -- Create autocommands
  vim.cmd([[
  augroup VimLoaderEvents
    autocmd FileType * call luaeval("require'vload/loader'.load_locals")(str2nr(expand('<abuf>')))
    autocmd BufDelete * call luaeval("require'vload/loader'.unload_locals")(str2nr(expand('<abuf>')))
  augroup END
  ]])
end

-- Changes vload_weight and loads additional global modules, Changes not
-- affect loaded buffers. To update specific buffer reset filetype local
-- option
function M.set_weight(weight)
  util.notify("Setting new weight to " .. weight, 1)
  local last_weight = data.vload_weight

  -- Update data
  data.vload_weight = weight

  if last_weight < weight then
    -- Load additional global modules
    loader.load_globals()
  end
end

-- Expose module functionality
return M
